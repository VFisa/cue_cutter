# README #
Script that cuts single music file into separate MP3s based on CUE within the folder.

### Requirements ###
* Python3
* FFMpeg

### Set up? ###
* (MAC) install Homebrew
* brew install python3
* brew install ffmpeg

### Source ###

* Inspired by Luka Zakrajšek
* https://gist.github.com/bancek
