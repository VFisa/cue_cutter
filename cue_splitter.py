"__author__ = 'Martin Fiser'"
"__credits__ = 'Twitter: @VFisa'"
"__source__ = 'Luka Zakrajšek, https://gist.github.com/bancek'"

import os
import sys
from pathlib import Path


## Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
print(script_path)


def gen_script(cue_content):
    """
    Slice file
    """

    general = {}
    tracks = []
    script = []
    current_file = None

    for line in cue_content:
        if line.startswith('REM GENRE '):
            general['genre'] = ' '.join(line.split(' ')[2:])
        if line.startswith('REM DATE '):
            general['date'] = ' '.join(line.split(' ')[2:])
        if line.startswith('PERFORMER '):
            general['artist'] = ' '.join(line.split(' ')[1:]).replace('"', '')
        if line.startswith('TITLE '):
            general['album'] = ' '.join(line.split(' ')[1:]).replace('"', '')
        if line.startswith('FILE '):
            current_file = ' '.join(line.split(' ')[1:-1]).replace('"', '')
        
        if line.startswith('  TRACK '):
            track = general.copy()
            track['track'] = int(line.strip().split(' ')[1], 10)

            tracks.append(track)

        if line.startswith('    TITLE '):
            tracks[-1]['title'] = ' '.join(line.strip().split(' ')[1:]).replace('"', '')
        if line.startswith('    PERFORMER '):
            tracks[-1]['artist'] = ' '.join(line.strip().split(' ')[1:]).replace('"', '')
        if line.startswith('    INDEX 01 '):
            t = list(map(int, ' '.join(line.strip().split(' ')[2:]).replace('"', '').split(':')))
            tracks[-1]['start'] = 60 * t[0] + t[1] + t[2] / 100.0

    for i in range(len(tracks)):
        if i != len(tracks) - 1:
            tracks[i]['duration'] = tracks[i + 1]['start'] - tracks[i]['start']

    print(tracks)

    for track in tracks:
        metadata = {
            'artist': track['artist'],
            'title': track['title'],
            'album': track['album'],
            'track': str(track['track']) + '/' + str(len(tracks))
        }

        if 'genre' in track:
            metadata['genre'] = track['genre']
        if 'date' in track:
            metadata['date'] = track['date']

        cmd = 'ffmpeg'
        cmd += ' -i "%s"' % current_file
        cmd += ' -b:a 320k'
        cmd += ' -ss %.2d:%.2d:%.2d' % (track['start'] / 60 / 60, track['start'] / 60 % 60, int(track['start'] % 60))

        if 'duration' in track:
            cmd += ' -t %.2d:%.2d:%.2d' % (track['duration'] / 60 / 60, track['duration'] / 60 % 60, int(track['duration'] % 60))

        cmd += ' ' + ' '.join('-metadata %s="%s"' % (k, v) for (k, v) in metadata.items())
        cmd += ' "%.2d - %s - %s.mp3"' % (track['track'], track['artist'], track['title'])

        #print(cmd)
        script.append(cmd)

    return script


def save_script(script):
    """
    Save script as shell file
    """

    with open("split_script.sh", "w") as out_file:
        for a in script:
            out_file.writelines(a)
            out_file.writelines(" ; ")

    out_file.close()

    print("Script generated within folder.")


if __name__ == "__main__":
    """
    Generate script for CUE music file split
    Converted to Python 3 environment (map & print fixes)
    """

    # first argument is a script name
    try:
        filename = sys.argv[1]
    except:
        print("filename argument missing! Exit.")
        print("call the script such as: python3 cue_splitter.py CDImage_flac.cue")
        sys.exit(1)
    
    # argument test
    if len(sys.argv) > 2:
        print("Too many arguments, using just the first one for filename!")

    # file exist test
    my_file = Path(str(script_path)+"/"+filename)
    print(my_file)
    if my_file.is_file():
        print("CUE file found!")
    else:
        print("CUE file could not be found in the folder! Exit.")
        sys.exit(1)

    # 'CDImage_flac.cue'
    cue_file = filename

    cue_content = open(cue_file).read().splitlines()

    # script assembly
    script = gen_script(cue_content)

    # produce sh file
    save_script(script)

    # make script executable
    os.system('chmod +x split_script.sh')

    # run script
    os.system('./split_script.sh')
    
    print("script completed.")
